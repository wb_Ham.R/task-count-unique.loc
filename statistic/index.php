<?php
// проверка и запись уникального посетителя

// имя cookie для счетчика
$name_cookie = 'count';

// debug
// setcookie($name_cookie, "", time() - 3600);

// проверка наличия cookie
if (!isset($_COOKIE[$name_cookie]) || time() - $_COOKIE[$name_cookie] > 24 * 3600) {
    setcookie($name_cookie, time(), time() + 10 * 365 * 24 * 3600);
    new_unique();
}

// запись в базу уникального посетителя
function new_unique() {
    $path = 'statistic/data/' . date('Y') . '/' . date('m') . '/' . date('d') . '/';
    $file = date('h');
    if (!is_dir($path)) {
        mkdir($path, 0755, true);
    }
    $hash = md5($_SERVER['REMOTE_ADDR'] . time());
    file_put_contents($path . $file, $hash . ',', FILE_APPEND | LOCK_EX);
}
