<?php
// обработка данных и выдача статистики
// формат запроса get_stat.php?sdate=2018/01/01&edate=2018/10/24

// время начала работы скрипта
$time_start = time();

// начальная, конечная дата статистики
$start = htmlspecialchars($_GET['sdate']) ? : date('Y/m/d');
$end = htmlspecialchars($_GET['edate']) ? : date('Y/m/d');

// данные по лог-файлу
$log_path = '../log/';
$log_file = date('Y-m-d') . '.log';
$dump = date('H:i:s') . ' - IP: ' . $_SERVER['REMOTE_ADDR'] . ', start date: ' . $start . ', end date: ' . $end;

// проверка дат, переданных в GET. если неправильные - вывод ошибки, запись лога
if (!verifyDate($start, $end)) {
    echo json_encode(['error' => $message]);
    dumpLog($dump, $log_file, $log_path, $time_start);
    die;
}

// вывод статистики
$arData = getData($start, $end);
$result = json_encode($arData);
dumpLog($dump, $log_file, $log_path, $time_start);
echo $result;

// проверка дат
function verifyDate ($start, $end) {
    $arStart = explode('/', $start);
    $arEnd = explode('/', $end);
    if ($start <= $end &&
        checkDate($arStart[1], $arStart[2], $arStart[0]) &&
        checkDate($arEnd[1], $arEnd[2], $arEnd[0])) {
        return true;
    }
    return false;
}

// сбор статистики из файлов
function getData ($start, $end) {
    $iter = function ($acc, $curDate, $arHashExists) use ($end, &$iter) {
        $newAcc = $acc;
        if ($curDate > $end) {
            return $acc;
        }
        list ($year, $month, $day) = explode('/', $curDate);
        $path = '../data/' . $year . '/' . $month . '/' . $day . '/';
        if (is_dir($path)) {

            //получение списка файлов
            $skip = array('.', '..');
            $files = scandir($path);
            foreach($files as $file) {
                if(in_array($file, $skip) || !is_file($path . $file)) {
                    continue;
                }
                $arData = explode(',', file_get_contents($path . $file));
                $stat_hour = array_reduce($arData, function ($acc, $item) use (&$arHashExists) {
                    if ($arHashExists[$item]) {
                        return $acc;
                    }
                    $arHashExists[$item] = true;
                    $acc[] = $item;
                    return $acc;
                }, []);
                $newAcc[$year][$month][$day][$file] = sizeOf($stat_hour);
            }
        }
        $str = strtotime($curDate) + 86400;
        $nextDate = date('Y/m/d', $str);
        return $iter($newAcc, $nextDate, $arHashExists);
    };
    return $iter([], $start, []);
}

// сохранение лога в файл
function dumpLog ($dump, $log_file, $path, $time_start) {
    if (!is_dir($path)) {
        mkdir($path, 0755, true);
    }
    $time_script = time() - $time_start;
    file_put_contents($path . $log_file,
        $dump . "\nВремя выполнения запроса: {$time_script} сек. \n\n", FILE_APPEND | LOCK_EX);
}